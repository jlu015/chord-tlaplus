VERIFICATION OF THE CHORD PROTOCOL IN TLA+
============================================
### Jørgen Aarmo Lund 15.05.2019
### INF-3990 Master's Thesis in Computer Science

This repository contains the TLA+ specifications
of the Chord protocol, as well as specifications of counterexamples
against its claimed safety properties, from the thesis
"Verification of the Chord protocol in TLA+".


## Summary

The protocol specifications are in the `spec/` directory:

* `SynchronousChord.tla` contains the specification of synchronous Chord,
    with granularity at the event level and no failures
* `PureJoinChord.tla` contains the specification of pure-join Chord,
    with asynchronous messaging between nodes and no failures
* `Chord2003.tla` contains the full specification of Chord,
    with asynchronous messaging, fault tolerance and fail-stop failures

The `spec/Counterexamples` directory contains the TLA+ specifications
of Zave's counterexamples from her 2012 analysis of the Chord protocol.

## Demo

The script `spec/test.sh` model-checks the specifications
for small N and low trace depths, and verifies
that the specifications admit Zave's counterexamples
(enclosed in spec/Counterexamples/). Java must be installed,
and a copy of `tla2tools.jar` from

https://github.com/tlaplus/tlaplus/releases/latest

must be placed in the `spec/` directory. If `test.sh` fails with `no main manifest`,
you may have to download a nightly build from

http://nightly.tlapl.us/products/

Alternatively, these tests can be run from the container `corporesano/chord-tlaplus-demo`:

`$ docker run --rm -it corporesano/chord-tlaplus-demo`

The container can be built from the enclosed Dockerfile as well:

`$ docker build -t chord-tlaplus-demo .`

`$ docker run --rm -it chord-tlaplus-demo`

## Measurements

The script `spec/stats.sh` counts the number of states explored
while model-checking the specifications for various configurations,
and measures the average runtime for model-checking.
(Beware that this takes the better part of a weekend on the author's workstation!)

It requires Java 8 to be installed, along with the "multitime" utility found at
https://tratt.net/laurie/src/multitime/

`stats.sh` uses TLC configuration files from the `spec/Statistics` directory 
for the performance evaluation.

#!/bin/bash
set -euo pipefail

# VERIFICATION OF THE CHORD PROTOCOL IN TLA+
# INF-3990 Master's Thesis in Computer Science
# Jørgen Aarmo Lund 15.05.2019
# 
# This script collects runtime statistics from model-checking the
# specification for various configurations.
#
# This repository packages version 2.13 (rev: 14440ac)
# of the TLC model checker.
#
# This script depends on having Java installed,
# along with the "multitime" utility found at
# https://tratt.net/laurie/src/multitime/
# 
# See README.txt in main repo directory for a containerized demo.
JAVA_CMD="java" 
TLC_CMD="-jar tla2tools.jar"

# These arguments are normally set by TLA+ Toolbox,
# and may need to be changed for your PC -
# in particular, newer Java versions may require the UseParallelGC option.
JAVA_ARGS="-XX:MaxDirectMemorySize=10726m -Xmx5365m -XX:+IgnoreUnrecognizedVMOptions"
TLC_ARGS="-cleanup -workers 4"

# How many trials to run for each configuration
MULTITIME_TRIALS="5"

for n in {2..6}; do
    echo "==============================================================="
    echo "Counting states for synchronous Chord for N=${n}"
    echo "==============================================================="
    ${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -config Statistics/SynchronousChord-Safety-${n}.cfg SynchronousChord.tla 
done

for n in {2..6}; do
    echo "==============================================================="
    echo "Counting states for pure-join Chord with asynchronous messaging for N=${n}"
    echo "==============================================================="
    for d in 20 25 30; do
        echo "--> Checking traces of length ${d}"
        ${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -config Statistics/PureJoinChord-${n}.cfg -dfid ${d} PureJoinChord.tla 
    done
done

for n in {2..6}; do
    echo "==============================================================="
    echo "Counting states for Stoica's Chord for N=${n}"
    echo "==============================================================="
    for d in 20 25 30; do
        echo "--> Checking traces of length ${d}"
        ${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -deadlock -config Statistics/Chord2003-${n}.cfg -dfid ${d} Chord2003.tla 
    done
done

for n in {2..6}; do
    echo "==============================================================="
    echo "Checking average runtime of synchronous Chord (safety only) for N=${n}"
    echo "==============================================================="
    multitime -q -n ${MULTITIME_TRIALS} ${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -config Statistics/SynchronousChord-Safety-${n}.cfg SynchronousChord.tla 
done

for n in {2..6}; do
    echo "==============================================================="
    echo "Checking average runtime of synchronous Chord (safety and liveness) for N=${n}"
    echo "==============================================================="
    multitime -q -n ${MULTITIME_TRIALS} ${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -config Statistics/SynchronousChord-Liveness-${n}.cfg SynchronousChord.tla 
done

for n in {2..6}; do
    echo "==============================================================="
    echo "Checking average runtime of pure-join Chord with asynchronous messaging for N=${n}"
    echo "==============================================================="
    for d in 20 25 30; do
        echo "--> Checking traces of length ${d}"
        multitime -q -n ${MULTITIME_TRIALS} ${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -config Statistics/PureJoinChord-${n}.cfg -dfid ${d} PureJoinChord.tla 
    done
done

for n in {2..6}; do
    echo "==============================================================="
    echo "Checking average runtime of Stoica's Chord for N=${n}"
    echo "==============================================================="
    for d in 20 25 30; do
        echo "--> Checking traces of length ${d}"
        multitime -q -n ${MULTITIME_TRIALS} ${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -deadlock -config Statistics/Chord2003-${n}.cfg -dfid ${d} Chord2003.tla 
    done
done

echo "==============================================================="
echo "FINISHED!"
echo "==============================================================="
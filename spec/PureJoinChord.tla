--------------------------- MODULE PureJoinChord ---------------------------
(* Models the pure-join Chord stabilization algorithm
as specified in the 2001 SIGCOMM paper, using asynchronous communication
between nodes. *)

EXTENDS Integers, FiniteSets, Sequences, TLC
CONSTANTS 
    N, (* Number of nodes in the model *)
    UseInitialRing (* FALSE if we should examine all possible configurations of successor pointers *)
ASSUME FiniteNodesAssumption == N \in Nat \ {0}
ASSUME UseInitialRingAssumption == UseInitialRing \in BOOLEAN

ProcSet == 1..N

(* We amend the previous specification to include unordered sets
representing the messages sent between nodes: *)
VARIABLES Successor,
    Predecessor,
    HasJoined,
    HasPredecessor,
    SuccessorAnswers, 
    SuccessorRequests,
    PredecessorAnswers, 
    PredecessorRequests,
    Notifications
vars == <<Successor, Predecessor, HasJoined, HasPredecessor,
    SuccessorAnswers, SuccessorRequests,
    PredecessorAnswers, PredecessorRequests,
    Notifications>>

(* BetweenInclusive is true if x \in [a, b] (mod N).*)
BetweenInclusive(x, a, b) == ((x - a) % N) <= ((b - a) % N)

(* BetweenHalfOpen is true if x \in (a, b] (mod N).*)
BetweenHalfOpen(x, a, b) == 
    /\ ((x - a) % N) <= ((b - a) % N) /\ x # a

(* BetweenExclusive is true if x \in (a, b) (mod N) *)
BetweenExclusive(x, a, b) == 
    \/ /\ ((x - a) % N) <= ((b - a) % N) /\ x # a /\ x # b
    \/ /\ a = b

(* We keep the safety properties from the synchronous Chord specification
(see Appendix A): *)        
ReachableSuccessors(x) ==
    LET RECURSIVE Reachable(_,_)
        Reachable(n,i) ==
            IF i = 0 \/ ~HasJoined[n]
            THEN {}
            ELSE Reachable(Successor[n], i-1) \cup {Successor[n]}
    IN Reachable(x, N)
    
FormsRing(x) == x \in ReachableSuccessors(x)

IsRingAppendage(x) == 
    \E succ \in ReachableSuccessors(Successor[x]): FormsRing(succ)

ConnectedAppendages == 
    \A self \in ProcSet: IsRingAppendage(self)

AtLeastOneRing == \E x \in ProcSet: FormsRing(x)

AtMostOneRing == ~\E x \in ProcSet: 
    /\ FormsRing(x)
    /\ \E y \in ProcSet:
        /\ FormsRing(y)
        /\ ReachableSuccessors(x) # ReachableSuccessors(y)

OrderedRing == 
    LET ring == CHOOSE x \in ProcSet : FormsRing(x) IN
        /\ \A self \in ReachableSuccessors(ring):
            \/ self < Successor[self]
            \/ ~\E n \in ReachableSuccessors(ring): n > Successor[n] /\ self # n

ValidRing == 
    /\ AtLeastOneRing
    /\ AtMostOneRing
    /\ ConnectedAppendages
    /\ OrderedRing

(* To be a valid initial state, at least one node must have
a predecessor pointer set. To ensure we only explore initial states
which are reachable through normal operation, we also require that
nodes' predecessor pointers are either unset, or lead to 
nodes leading back to them.  *)        
ValidInitialPredecessors ==
    /\ \E p \in ProcSet: HasPredecessor[p]
    /\ \A p \in ProcSet:
        \/ ~HasPredecessor[p]
        \/ /\ Successor[Predecessor[p]] = p
           /\ HasJoined[Predecessor[p]]

(* Since exploring every possible configuration of successor
pointers quickly leads to a state space explosion, 
allow exploring a subset of the initial states
where one node forms a single-node ring: *)
InitialRing ==
    /\ \E Start \in ProcSet:
        /\ Successor = [self \in ProcSet |-> Start]
        /\ Predecessor = [self \in ProcSet |-> Start]
        /\ HasJoined = [self \in ProcSet |-> self = Start]
        /\ HasPredecessor = [self \in ProcSet |-> self = Start]
    
Init == 
    /\ IF UseInitialRing 
        THEN InitialRing
        ELSE
        /\ Successor \in [ProcSet -> ProcSet]
        /\ Predecessor \in [ProcSet -> ProcSet]
        /\ HasJoined \in [ProcSet -> BOOLEAN]
        /\ HasPredecessor \in [ProcSet -> BOOLEAN]
    /\ SuccessorAnswers = [self \in ProcSet |-> {}]
    /\ SuccessorRequests = [self \in ProcSet |-> {}]
    /\ PredecessorAnswers = [self \in ProcSet |-> {}]
    /\ PredecessorRequests = [self \in ProcSet |-> {}]
    /\ Notifications = [self \in ProcSet |-> {}]
    /\ ValidRing
    /\ ValidInitialPredecessors



(* Next, we introduce new actions and amend previous ones to 
pass messages instead of retrieving the state directly. *)

(* Recursively find the successor for a given identifier. *)
FindSuccessor(self) ==
    /\ HasJoined[self]
    /\ UNCHANGED <<Successor, Predecessor, HasJoined, 
        HasPredecessor, PredecessorAnswers, PredecessorRequests,
        Notifications>>
    /\ \E Request \in SuccessorRequests[self]:
        (* If the key lies in the interval (n, successor(n)],
           the key belongs to node n's successor: *)
        IF  \/ BetweenHalfOpen(Request.id, self, Successor[self]) 
            \/ self = Successor[self]
        THEN
            (* Remove this query from the set of requests. *)
            /\ SuccessorRequests' = [SuccessorRequests 
                EXCEPT ![self] = @ \ {Request}] 
            (* Return the answer to the original sender. *)
            /\ SuccessorAnswers' = [SuccessorAnswers 
                EXCEPT ![Request.origin] =
                @ \union {[id |-> Request.id, successor |-> Successor[self]]}]
        ELSE
            /\ SuccessorRequests' = [SuccessorRequests 
                EXCEPT ![Successor[self]] =
                @ \union {Request}, ![self] = @ \ {Request}]
            /\ SuccessorAnswers' = SuccessorAnswers
                
(* Join the network by requesting the successor of this node's identifier
from a known member of the network: *)
Join(self) == 
    /\ ~HasJoined[self]
    /\ Cardinality(SuccessorAnswers[self]) = 0
    /\ SuccessorRequests' =
        [SuccessorRequests EXCEPT ![Successor[self]] =
        @ \union {[origin |-> self, id |-> self]}]
    /\ UNCHANGED <<Successor, Predecessor, HasJoined, 
        HasPredecessor, SuccessorAnswers, PredecessorAnswers, 
        PredecessorRequests, Notifications>>

(* On receiving the successor for this node's identifier, 
set it as the successor: *)
FinishJoin(self) == 
    /\ ~HasJoined[self]
    /\ UNCHANGED <<Predecessor, HasPredecessor,
        SuccessorRequests,
        PredecessorAnswers, PredecessorRequests,
        Notifications>>
    /\ \E Answer \in SuccessorAnswers[self]:
        /\ Successor' = [Successor EXCEPT ![self] = Answer.successor]
        /\ HasJoined' = [HasJoined EXCEPT ![self] = TRUE]
        /\ SuccessorAnswers' = [SuccessorAnswers EXCEPT ![self] = @ \ {Answer}]

(* Periodically verify a node's immediate successor,
and tell the successor about the node, using the algorithm
from Figure 7 of the SIGCOMM paper. *)
Stabilize(self) ==
    /\ HasJoined[self]
    /\ UNCHANGED <<Successor, Predecessor, HasJoined, HasPredecessor,
        SuccessorAnswers, SuccessorRequests,
        PredecessorAnswers,
        Notifications>>
    /\ PredecessorRequests' = [PredecessorRequests EXCEPT ![Successor[self]] =
        @ \union {[origin |-> self]}]

(* Return the node's current predecessor. *)
GetPredecessor(self) ==
    /\ HasJoined[self]
    /\ HasPredecessor[self]
    /\ UNCHANGED <<Successor, Predecessor, HasJoined, HasPredecessor,
        SuccessorAnswers, SuccessorRequests,
        Notifications>>
    (* If there is a request, return our current predecessor: *)
    /\ \E Request \in PredecessorRequests[self]:
        /\ PredecessorRequests' = 
            [PredecessorRequests EXCEPT ![self] = @ \ {Request}]
        /\ PredecessorAnswers' =
            [PredecessorAnswers EXCEPT ![Request.origin] =
            @ \union {[id |-> self, predecessor |-> Predecessor[self]]}]
            
(* Once the node has received its successor's predecessor,
 determine whether it is a closer successor,
 adopting it as our new successor if it is. *)
FinishStabilize(self) ==
    /\ HasJoined[self]
    /\ UNCHANGED <<Predecessor, HasJoined, HasPredecessor,
        SuccessorAnswers, SuccessorRequests,
        PredecessorRequests>>
    /\ \E Answer \in PredecessorAnswers[self]:
        IF 
            \/ BetweenExclusive(Answer.predecessor, self, Successor[self])
        THEN
            /\ Successor' = [Successor EXCEPT ![self] = Answer.predecessor]
            /\ PredecessorAnswers' = 
                [PredecessorAnswers EXCEPT ![self] = {}]
            /\ Notifications' = [Notifications EXCEPT ![Answer.predecessor] =
                @ \union {[origin |-> self]}]
        ELSE
            /\ Successor' = Successor
            /\ PredecessorAnswers' = 
                [PredecessorAnswers EXCEPT ![self] = {}]
            /\ Notifications' = [Notifications EXCEPT ![Successor[self]] =
                @ \union {[origin |-> self]}]

(* Accept a notification from another node
which thinks it might be our predecessor. *)
Notify(self) ==
    /\ HasJoined[self]
    /\ UNCHANGED <<Successor, HasJoined,
        SuccessorAnswers, SuccessorRequests,
        PredecessorAnswers, PredecessorRequests>>
    /\ \E Note \in Notifications[self]:
        /\ IF 
            \/ ~HasPredecessor[self]
            \/ BetweenExclusive(Note.origin, Predecessor[self], self)
            THEN            
            /\ HasPredecessor' = [HasPredecessor EXCEPT ![self] = TRUE]
            /\ Predecessor' = [Predecessor EXCEPT ![self] = Note.origin]
            /\ Notifications' = [Notifications EXCEPT ![self] = @ \ {Note}]
            ELSE
            /\ UNCHANGED <<HasPredecessor, Predecessor>>
            /\ Notifications' = [Notifications EXCEPT ![self] = @ \ {Note}]
    
Node(self) == 
    \/ FindSuccessor(self)
    \/ Join(self)
    \/ FinishJoin(self)
    \/ Stabilize(self)
    \/ GetPredecessor(self)
    \/ FinishStabilize(self)
    \/ Notify(self)

Next == (\E self \in 1..N: Node(self))

Spec == /\ Init /\ [][Next]_vars
        /\ \A self \in 1..N : WF_vars(Node(self))
    
TypeOK == 
    /\ \A self \in ProcSet: 
        /\ Successor[self] \in ProcSet
        /\ Predecessor[self] \in ProcSet
        /\ HasJoined[self] \in BOOLEAN
        /\ HasPredecessor[self] \in BOOLEAN
        /\ IsFiniteSet(SuccessorAnswers[self])
        /\ IsFiniteSet(SuccessorRequests[self])
        /\ IsFiniteSet(PredecessorAnswers[self])
        /\ IsFiniteSet(PredecessorRequests[self])
        /\ IsFiniteSet(Notifications[self])

(* As a sanity check, test that we can reach a weakly ideal ring.
Checking the NoRingBecomesIdeal invariant should return a violation. *)
RingIsWeaklyIdeal == 
    \A self \in ProcSet: 
        /\ HasJoined[self]
        /\ HasPredecessor[self]
        /\ Successor[Predecessor[self]] = self

NoRingBecomesIdeal == ~RingIsWeaklyIdeal

=============================================================================

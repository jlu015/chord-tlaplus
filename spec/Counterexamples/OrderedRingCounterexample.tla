--------------------- MODULE OrderedRingCounterexample ---------------------
(* Models the counterexample to the OrderedRing property
from (4a) in definition 5.6 in the 2002 paper by Liben-Nowell et al.,
which states that

    "the cycle is non-loopy."

i.e. the cycle maintains identifier ordering.
This property is the core assumption behind finger tables. 
*)
EXTENDS Chord2003
ASSUME N = 6
ASSUME SuccessorsPerNode = 2
ASSUME UseInitialRing = FALSE
VARIABLES Time

ExampleInit == 
    /\ Time = 0
    /\ Successors = <<<<3>>, <<1>>, <<5>>, <<3>>, <<1>>, <<5>>>>
    /\ Predecessor = <<5, 1, 1, 1, 3, 1>>
    /\ HasJoined = <<TRUE, FALSE, TRUE, FALSE, TRUE, FALSE>>
    /\ HasPredecessor = <<TRUE, FALSE, TRUE, FALSE, TRUE, FALSE>>
    /\ HasFailed = <<FALSE, FALSE, FALSE, FALSE, FALSE, FALSE>>
    /\ SuccessorRequests = [self \in ProcSet |-> {}]
    /\ SuccessorAnswers = [self \in ProcSet |-> {}]
    /\ PredecessorRequests = [self \in ProcSet |-> {}]
    /\ PredecessorAnswers = [self \in ProcSet |-> {}]
    /\ Notifications = [self \in ProcSet |-> {}]
    /\ Init

Step(y) == 
    /\ Time = y 
    /\ Time' = y + 1
           
Done(y) == 
    /\ Time = y
    /\ UNCHANGED <<Time>>
    /\ UNCHANGED vars

(* Like the counterexample to the "at most one ring" property (1b),
this counterexample relies on FinishStabilize preserving the rest
of the successor list when stabilizing. *)
FaultyFinishStabilize(self) ==
    /\ HasJoined[self]
    /\ UNCHANGED <<Predecessor, HasJoined, HasPredecessor, HasFailed,
        SuccessorAnswers, SuccessorRequests,
        PredecessorRequests>>
    /\ \E Answer \in PredecessorAnswers[self]:
        /\ ~HasFailed[Answer.predecessor]
        /\ IF BetweenExclusive(Answer.predecessor, self, Head(Successors[self]))
            THEN
                (* Preserve the rest of the successor list: *)
                /\ Successors' = [Successors EXCEPT ![self] = 
                    IF Len(Successors[self]) > 1
                    THEN <<Answer.predecessor>> \o Tail(Successors[self])
                    ELSE <<Answer.predecessor>>]
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Answer.predecessor] =
                    @ \union {[origin |-> self]}]
            ELSE
                /\ Successors' = Successors
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Head(Successors[self])] =
                    @ \union {[origin |-> self]}]
                    
(* Nodes 0, 5, 18, 21, 40, and 49 respectively correspond to 1, 2, 3, 4, 5, 6*)
ExampleNext == 
    (* Node 1, 3 and 5 form a ring. They update their successor lists: *)
    \/ /\ Step(0) /\ Init /\ FixSuccessorList(1)
    \/ /\ Step(1) /\ FixSuccessorList(3)
    \/ /\ Step(2) /\ FixSuccessorList(5)
    (* Nodes 2, 4 and 6 join: *)
    \/ /\ Step(3) /\ Join(2)
    \/ /\ Step(4) /\ FindSuccessor(1)
    \/ /\ Step(5) /\ FinishJoin(2)
    \/ /\ Step(6) /\ Stabilize(2)
    \/ /\ Step(7) /\ GetPredecessor(3)
    \/ /\ Step(8) /\ FaultyFinishStabilize(2)
    \/ /\ Step(9) /\ Join(4)
    \/ /\ Step(10) /\ FindSuccessor(3)
    \/ /\ Step(11) /\ FinishJoin(4)
    \/ /\ Step(12) /\ Stabilize(4)
    \/ /\ Step(13) /\ GetPredecessor(5)
    \/ /\ Step(14) /\ FaultyFinishStabilize(4)
    \/ /\ Step(15) /\ Join(6)
    \/ /\ Step(16) /\ FindSuccessor(5)
    \/ /\ Step(17) /\ FinishJoin(6)
    \/ /\ Step(18) /\ Stabilize(6)
    \/ /\ Step(19) /\ GetPredecessor(1)
    \/ /\ Step(20) /\ FaultyFinishStabilize(6)
    (* Nodes 2, 4, 6 are incorporated into the ring: *)
    \/ /\ Step(21) /\ Notify(1)
    \/ /\ Step(22) /\ Notify(3)
    \/ /\ Step(23) /\ Notify(5)
    \/ /\ Step(24) /\ Stabilize(1)
    \/ /\ Step(25) /\ GetPredecessor(3)
    \/ /\ Step(26) /\ FaultyFinishStabilize(1)
    \/ /\ Step(27) /\ Stabilize(3)
    \/ /\ Step(28) /\ GetPredecessor(5)
    \/ /\ Step(29) /\ FaultyFinishStabilize(3)
    \/ /\ Step(30) /\ Stabilize(5)
    \/ /\ Step(31) /\ GetPredecessor(1)
    \/ /\ Step(32) /\ FaultyFinishStabilize(5)
    (* Nodes 2, 4, 6 fail: *)
    \/ /\ Step(33) /\ Fail(2)
    \/ /\ Step(34) /\ Fail(4)
    \/ /\ Step(35) /\ Fail(6)
    (* Nodes 1, 3, 5 repair their successor lists: *)
    \/ /\ Step(36) /\ FixSuccessor(1)
    \/ /\ Step(37) /\ FixSuccessor(3)
    \/ /\ Step(38) /\ FixSuccessor(5)
    (* The ring is now loopy: 1 -> 5 -> 3 -> 1 -> ... *)
    \/ /\ Done(39) /\ ~OrderedRing

Counterexample == ExampleInit /\ [][ExampleNext]_<<Time, vars>>



=============================================================================
\* Modification History
\* Last modified Thu May 09 19:46:06 CEST 2019 by lund
\* Created Wed Apr 17 13:56:07 CEST 2019 by lund

-------------------- MODULE OrderedMergesCounterexample --------------------
(* Models the counterexample to the OrderedMerges property,
which appears as (5c) in the 2002 paper by Liben-Nowell et al.,
which states that

    "if v is an appendage Au of the node u, then u is the first live cycle
     node following v."

Zave informally expresses this property as
    
    "an appendage merges into the ring at the right place in identifier order."
          
*)
EXTENDS Chord2003
ASSUME N = 4
ASSUME SuccessorsPerNode = 2
ASSUME UseInitialRing = TRUE
VARIABLES Time

ExampleInit == 
    /\ Time = 0
    /\ Successors = <<<<4>>, <<4>>, <<4>>, <<4>>>>
    /\ Predecessor = <<4, 4, 4, 4>>
    /\ HasJoined = <<FALSE, FALSE, FALSE, TRUE>>
    /\ HasPredecessor = <<FALSE, FALSE, FALSE, TRUE>>
    /\ HasFailed = [self \in ProcSet |-> FALSE]
    /\ SuccessorAnswers = [self \in ProcSet |-> {}]
    /\ SuccessorRequests = [self \in ProcSet |-> {}]
    /\ PredecessorAnswers = [self \in ProcSet |-> {}]
    /\ PredecessorRequests = [self \in ProcSet |-> {}]
    /\ Notifications = [self \in ProcSet |-> {}]

Step(y) == 
    /\ Time = y 
    /\ Time' = y + 1
           
Done(y) == 
    /\ Time = y
    /\ UNCHANGED <<Time>>
    /\ UNCHANGED vars

(* If OrderedMerges holds, node 2 should have node 3
as its successor after joining (so long as node 3 is reachable). *)
OrderedMerges ==
    \/ ~HasJoined[2]
    \/ ~(3 \in ReachableSuccessors(2))
    \/ Head(Successors[2]) = 3
    
(* Node 6, 8, 10, and 16 of Zave's example 
correspond to node 1, 2, 3 and 4 here. *)
ExampleNext == 
    (* Node 1 and 4 form a ring: *)
    \/ /\ Step(0) /\ Init /\ Join(1)
    \/ /\ Step(1) /\ FindSuccessor(4)
    \/ /\ Step(2) /\ FinishJoin(1)
    \/ /\ Step(3) /\ Stabilize(1)
    \/ /\ Step(4) /\ GetPredecessor(4)
    \/ /\ Step(5) /\ FinishStabilize(1)
    \/ /\ Step(6) /\ Notify(4)
    \/ /\ Step(7) /\ Stabilize(4)
    \/ /\ Step(8) /\ GetPredecessor(4)
    \/ /\ Step(9) /\ FinishStabilize(4)
    \/ /\ Step(10) /\ Notify(1)
    (* Node 2 and 3 join: *) 
    \/ /\ Step(11) /\ Join(2)
    \/ /\ Step(12) /\ Join(3)
    (* Node 4 responds to node 2 first: *)
    \/ /\ Step(13) /\ FindSuccessor(4) /\ [id |-> 2, origin |-> 2] \in SuccessorRequests'[1] 
    \/ /\ Step(14) /\ FindSuccessor(1)
    \/ /\ Step(15) /\ FinishJoin(2)
    \/ /\ Step(16) /\ Stabilize(2)
    \/ /\ Step(17) /\ GetPredecessor(4)
    \/ /\ Step(18) /\ FinishStabilize(2)
    \/ /\ Step(19) /\ Notify(4)
    (* Node 3 then finishes joining: *)
    \/ /\ Step(20) /\ FindSuccessor(4)
    \/ /\ Step(21) /\ FindSuccessor(1)
    \/ /\ Step(22) /\ FinishJoin(3)
    \/ /\ Step(23) /\ Stabilize(3)
    \/ /\ Step(24) /\ GetPredecessor(4)
    \/ /\ Step(25) /\ FinishStabilize(3)
    (* Node 3 is adopted as the new predecessor of node 4: *)
    \/ /\ Step(26) /\ Notify(4)
    \/ /\ Step(27) /\ Stabilize(1)
    \/ /\ Step(28) /\ GetPredecessor(4)
    \/ /\ Step(29) /\ FinishStabilize(1)
    \/ /\ Step(30) /\ Notify(3)
    (* Despite node 3 being in the ring, node 2 now has node 4
    as its initial successor: *)
    \/ /\ Done(31) /\ ~OrderedMerges
    
       

Counterexample == ExampleInit /\ [][ExampleNext]_<<Time, vars>>
=============================================================================
\* Modification History
\* Last modified Thu May 09 16:36:06 CEST 2019 by lund
\* Created Wed Apr 17 13:55:01 CEST 2019 by lund

-------------------- MODULE AtMostOneRingCounterexample --------------------
(* Models the counterexample to the AtMostOneRing property,
which is part of property (1) in definition 5.6 in the 2002 paper 
by Liben-Nowell et al., which states that "the network is connected".

AtMostOneRing establishes that there is at most one cycle. Otherwise,
we have multiple disjoint cycles, which cannot be merged by the
Chord stabilization protocol.          
*)
EXTENDS Chord2003, TLC
ASSUME N = 4
ASSUME SuccessorsPerNode = 2
ASSUME UseInitialRing = FALSE
VARIABLES Time

ExampleInit == 
    /\ Time = 0
    /\ Successors = <<<<2>>, <<4>>, <<4>>, <<2>>>>
    /\ Predecessor = <<4, 4, 4, 2>>
    /\ HasJoined = <<FALSE, TRUE, FALSE, TRUE>>
    /\ HasPredecessor = <<FALSE, TRUE, FALSE, TRUE>>
    /\ HasFailed = <<FALSE, FALSE, FALSE, FALSE>>
    /\ SuccessorRequests = <<{}, {}, {}, {}>>
    /\ SuccessorAnswers = <<{}, {}, {}, {}>>
    /\ PredecessorRequests = <<{}, {}, {}, {}>>
    /\ PredecessorAnswers = <<{}, {}, {}, {}>>
    /\ Notifications = <<{}, {}, {}, {}>>

Step(y) == 
    /\ Time = y 
    /\ Time' = y + 1
           
Done(y) == 
    /\ Time = y
    /\ UNCHANGED <<Time>>
    /\ UNCHANGED vars

(* Zave's counterexample relies on FinishStabilize preserving the rest
of the successor list when stabilizing. *)
FaultyFinishStabilize(self) ==
    /\ HasJoined[self]
    /\ UNCHANGED <<Predecessor, HasJoined, HasPredecessor, HasFailed,
        SuccessorAnswers, SuccessorRequests,
        PredecessorRequests>>
    /\ \E Answer \in PredecessorAnswers[self]:
        /\ ~HasFailed[Answer.predecessor]
        /\ IF BetweenExclusive(Answer.predecessor, self, Head(Successors[self]))
            THEN
                (* Preserve the rest of the successor list: *)
                /\ Successors' = [Successors EXCEPT ![self] = 
                    IF Len(Successors[self]) > 1
                    THEN <<Answer.predecessor>> \o Tail(Successors[self])
                    ELSE <<Answer.predecessor>>]
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Answer.predecessor] =
                    @ \union {[origin |-> self]}]
            ELSE
                /\ Successors' = Successors
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Head(Successors[self])] =
                    @ \union {[origin |-> self]}]

ExampleNext == 
    (* Node 2 and 4 form a ring,
       and adopt themselves as their second successor: *)
    \/ /\ Step(0) /\ FixSuccessorList(2)
    \/ /\ Step(1) /\ FixSuccessorList(4)
    (* Nodes 1 and 3 join node 2 and 3, respectively: *)
    \/ /\ Step(2) /\ Join(3)
    \/ /\ Step(3) /\ FindSuccessor(4)
    \/ /\ Step(4) /\ FindSuccessor(2)
    \/ /\ Step(5) /\ FinishJoin(3)
    \/ /\ Step(6) /\ Stabilize(3)
    \/ /\ Step(7) /\ GetPredecessor(4)
    \/ /\ Step(8) /\ FaultyFinishStabilize(3)
    \/ /\ Step(9) /\ Notify(4)
    \/ /\ Step(10) /\ Join(1)
    \/ /\ Step(11) /\ FindSuccessor(2)
    \/ /\ Step(12) /\ FindSuccessor(4)
    \/ /\ Step(13) /\ FinishJoin(1)
    \/ /\ Step(14) /\ Stabilize(1)
    \/ /\ Step(15) /\ GetPredecessor(2)
    \/ /\ Step(16) /\ FaultyFinishStabilize(1)
    \/ /\ Step(17) /\ Notify(2)
    \/ /\ Step(18) /\ Stabilize(2)
    \/ /\ Step(19) /\ GetPredecessor(4)
    \/ /\ Step(20) /\ FaultyFinishStabilize(2)
    \/ /\ Step(21) /\ Stabilize(4)
    \/ /\ Step(22) /\ GetPredecessor(2)
    \/ /\ Step(23) /\ FaultyFinishStabilize(4)
    (* Nodes 1 and 3 fail, *)
    \/ /\ Step(24) /\ Fail(1)
    \/ /\ Step(25) /\ Fail(3)
    (* Node 2 and 4 still have themselves as their
       second successors, and adopt themselves as their
       successors, leading to two rings: *)
    \/ /\ Step(26) /\ FixSuccessor(2)
    \/ /\ Step(27) /\ FixSuccessor(4)
    \/ /\ Done(28) /\ ~AtMostOneRing
    

Counterexample == ExampleInit /\ [][ExampleNext]_<<Time, vars>>

=============================================================================
\* Modification History
\* Last modified Thu May 09 15:55:42 CEST 2019 by lund
\* Created Wed Apr 17 13:56:21 CEST 2019 by lund

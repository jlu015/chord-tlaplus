----------------- MODULE ConnectedAppendagesCounterexample -----------------
(* Models the counterexample to the ConnectedAppendages property,
which is part of property (1) in definition 5.6 in the 2002 paper 
by Liben-Nowell et al., which states that "the network is connected".

ConnectedAppendages establishes that nodes are either appendages or
part of a ring. If a node is neither, it has no way of communicating
with the nodes in the ring.           
*)
EXTENDS Chord2003
ASSUME N = 4
ASSUME SuccessorsPerNode = 2
ASSUME UseInitialRing = TRUE
VARIABLES Time

ExampleInit == 
    /\ Time = 0
    /\ Successors = <<<<4>>, <<4>>, <<4>>, <<4>>>>
    /\ Predecessor = <<4, 4, 4, 4>>
    /\ HasJoined = <<FALSE, FALSE, FALSE, TRUE>>
    /\ HasPredecessor = <<FALSE, FALSE, FALSE, TRUE>>
    /\ HasFailed = <<FALSE, FALSE, FALSE, FALSE>>
    /\ SuccessorRequests = <<{}, {}, {}, {}>>
    /\ SuccessorAnswers = <<{}, {}, {}, {}>>
    /\ PredecessorRequests = <<{}, {}, {}, {}>>
    /\ PredecessorAnswers = <<{}, {}, {}, {}>>
    /\ Notifications = <<{}, {}, {}, {}>>

Step(y) == 
    /\ Time = y 
    /\ Time' = y + 1
           
Done(y) == 
    /\ Time = y
    /\ UNCHANGED <<Time>>
    /\ UNCHANGED vars

(* Zave's counterexample relies on a faulty specification of Join
that fails to check whether the new successor is live. *)
FaultyFinishJoin(self) == 
    /\ ~HasFailed[self]
    /\ ~HasJoined[self]
    /\ UNCHANGED <<Predecessor, HasPredecessor, HasFailed,
        SuccessorRequests,
        PredecessorAnswers, PredecessorRequests,
        Notifications>>
    (* Don't check whether the successor is live: *)
    /\ \E Answer \in SuccessorAnswers[self]:
        /\ Successors' = [Successors EXCEPT ![self] = <<Answer.successor>>]
        /\ HasJoined' = [HasJoined EXCEPT ![self] = TRUE]
        /\ SuccessorAnswers' = [SuccessorAnswers EXCEPT ![self] = @ \ {Answer}]
        
ExampleNext == 
    (* Node 1 and 4 form a ring: *)
    \/ /\ Step(0) /\ Init /\ Join(1)
    \/ /\ Step(1) /\ FindSuccessor(4)
    \/ /\ Step(2) /\ FaultyFinishJoin(1)
    \/ /\ Step(3) /\ Stabilize(1)
    \/ /\ Step(4) /\ GetPredecessor(4)
    \/ /\ Step(5) /\ FinishStabilize(1)
    \/ /\ Step(6) /\ Notify(4)
    \/ /\ Step(7) /\ Stabilize(4)
    \/ /\ Step(8) /\ GetPredecessor(4)
    \/ /\ Step(9) /\ FinishStabilize(4)
    \/ /\ Step(10) /\ Notify(1)
    (* Node 3 joins and is incorporated into the ring: *)
    \/ /\ Step(11) /\ Join(3)
    \/ /\ Step(12) /\ FindSuccessor(4)
    \/ /\ Step(13) /\ FindSuccessor(1)
    \/ /\ Step(14) /\ FaultyFinishJoin(3)
    \/ /\ Step(15) /\ Stabilize(3)
    \/ /\ Step(16) /\ GetPredecessor(4)
    \/ /\ Step(17) /\ FinishStabilize(3)
    \/ /\ Step(18) /\ Notify(4)
    \/ /\ Step(19) /\ Stabilize(1)
    \/ /\ Step(20) /\ GetPredecessor(4)
    \/ /\ Step(21) /\ FinishStabilize(1)
    \/ /\ Step(22) /\ Notify(3)
    (* Node 1 adopts node 4 as its second successor: *)
    \/ /\ Step(23) /\ FixSuccessorList(1)
    (* Node 2 joins: *)
    \/ /\ Step(24) /\ Join(2)
    \/ /\ Step(25) /\ FindSuccessor(4)
    \/ /\ Step(26) /\ FindSuccessor(1)
    (* Node 3 fails before node 2 can join: *)
    \/ /\ Step(27) /\ Fail(3)
    \/ /\ Step(28) /\ FaultyFinishJoin(2)
    \/ /\ Done(29) /\ ~ConnectedAppendages
    
    

Counterexample == ExampleInit /\ [][ExampleNext]_<<Time, vars>>    
=============================================================================
\* Modification History
\* Last modified Thu May 09 21:19:39 CEST 2019 by lund
\* Created Wed Apr 17 13:55:55 CEST 2019 by lund

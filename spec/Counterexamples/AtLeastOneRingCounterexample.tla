-------------------- MODULE AtLeastOneRingCounterexample --------------------
(* Models the counterexample to the AtLeastOneRing property,
which is part of property (1) in definition 5.6 in the 2002 paper 
by Liben-Nowell et al., which states that "the network is connected".

AtLeastOneRing establishes that there is at least one node forming a cycle
(such that it can reach itself through following successor pointers).
Otherwise, there is at least one node without a successor pointer,
which means it cannot stabilize or perform lookups.  
*)
EXTENDS Chord2003
ASSUME N = 3
ASSUME SuccessorsPerNode = 2
ASSUME UseInitialRing = TRUE
VARIABLES Time

ExampleInit == 
    /\ Time = 0
    /\ Successors = <<<<3>>, <<3>>, <<3>>>>
    /\ Predecessor = <<3, 3, 3>>
    /\ HasJoined = <<FALSE, FALSE, TRUE>>
    /\ HasPredecessor = <<FALSE, FALSE, TRUE>>
    /\ HasFailed = [self \in ProcSet |-> FALSE]
    /\ SuccessorAnswers = [self \in ProcSet |-> {}]
    /\ SuccessorRequests = [self \in ProcSet |-> {}]
    /\ PredecessorAnswers = [self \in ProcSet |-> {}]
    /\ PredecessorRequests = [self \in ProcSet |-> {}]
    /\ Notifications = [self \in ProcSet |-> {}]

Step(y) == 
    /\ Time = y 
    /\ Time' = y + 1
           
Done(y) == 
    /\ Time = y
    /\ UNCHANGED <<Time>>
    /\ UNCHANGED vars

(* Zave's counterexample relies on Stabilize not checking whether the
node is live before adopting it as a successor.*)
FaultyFinishStabilize(self) ==
    /\ ~HasFailed[self]
    /\ HasJoined[self]
    /\ UNCHANGED <<Predecessor, HasJoined, HasPredecessor, HasFailed,
        SuccessorAnswers, SuccessorRequests,
        PredecessorRequests>>
    (* Deliberately omit the check that determines whether 
    the new successor is live: *)
    /\ \E Answer \in PredecessorAnswers[self]:
        /\ IF BetweenExclusive(Answer.predecessor, self, Head(Successors[self]))
            THEN
                /\ Successors' = [Successors EXCEPT ![self] = <<Answer.predecessor>>]
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Answer.predecessor] =
                    @ \union {[origin |-> self]}]
            ELSE
                /\ Successors' = Successors
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Head(Successors[self])] =
                    @ \union {[origin |-> self]}]

ExampleNext == 
    (* Node 1 and 3 form a ring: *)
    \/ /\ Step(0) /\ Init /\ Join(1)
    \/ /\ Step(1) /\ FindSuccessor(3)
    \/ /\ Step(2) /\ FinishJoin(1)
    \/ /\ Step(3) /\ Stabilize(1)
    \/ /\ Step(4) /\ GetPredecessor(3)
    \/ /\ Step(5) /\ FaultyFinishStabilize(1)
    \/ /\ Step(6) /\ Notify(3)
    \/ /\ Step(7) /\ Stabilize(3)
    \/ /\ Step(8) /\ GetPredecessor(3)
    \/ /\ Step(9) /\ FaultyFinishStabilize(3)
    \/ /\ Step(10) /\ Notify(1)
    (* Node 2 joins the ring, and becomes the predecessor of node 3: *)
    \/ /\ Step(11) /\ Join(2)
    \/ /\ Step(12) /\ FindSuccessor(3)
    \/ /\ Step(13) /\ FindSuccessor(1)
    \/ /\ Step(14) /\ FinishJoin(2)
    \/ /\ Step(15) /\ Stabilize(2)
    \/ /\ Step(16) /\ GetPredecessor(3)
    \/ /\ Step(17) /\ FaultyFinishStabilize(2)
    \/ /\ Step(18) /\ Notify(3)
    (* Node 1 begins stabilizing, *)
    \/ /\ Step(19) /\ Stabilize(1)
    \/ /\ Step(20) /\ GetPredecessor(3)
    (* node 2 fails, *)
    \/ /\ Step(21) /\ Fail(2)
    (* but node 1 does not check whether node 2 is live before
    adopting it as its successor: *)
    \/ /\ Step(22) /\ FaultyFinishStabilize(1)
    \/ /\ Step(23) /\ FixPredecessor(3)
    \/ /\ Done(24) /\ ~AtLeastOneRing 
    
Counterexample == ExampleInit /\ [][ExampleNext]_<<Time, vars>>
    
=============================================================================
\* Modification History
\* Last modified Thu May 09 15:49:49 CEST 2019 by lund
\* Created Wed Apr 17 13:56:39 CEST 2019 by lund

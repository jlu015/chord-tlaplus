------------------ MODULE ValidSuccessorListCounterexample ------------------
(* Models the counterexample to the ValidSuccessorList property,
which appears as (5d) in Definition 5.6 from the 2002 paper by Liben-Nowell et al.,
which states that
    
    "if the successor list of [the successor of node u] skips over a live node v,
     then v is not in the [successor list of node u]"

Zave states this property as

    "if v and w are members, and w's successor list skips over v, then v
    is not in the successor list of any immediate antecedent of w."
             
*)
EXTENDS Chord2003
ASSUME N = 5
ASSUME SuccessorsPerNode = 2
ASSUME UseInitialRing = TRUE
VARIABLES Time

ExampleInit == 
    /\ Time = 0
    /\ SuccessorAnswers = <<{}, {}, {}, {}, {}>>
    /\ HasPredecessor = [self \in ProcSet |-> self = 3]
    /\ Successors = [self \in ProcSet |-> <<3>>]
    /\ Predecessor = [self \in ProcSet |-> 3]
    /\ SuccessorRequests = [self \in ProcSet |-> {}]
    /\ PredecessorAnswers = [self \in ProcSet |-> {}]
    /\ HasJoined = [self \in ProcSet |-> self = 3]
    /\ Notifications = [self \in ProcSet |-> {}]
    /\ HasFailed = [self \in ProcSet |-> FALSE]
    /\ PredecessorRequests = [self \in ProcSet |-> {}]

Step(y) == 
    /\ Time = y 
    /\ Time' = y + 1
           
Done(y) == 
    /\ Time = y
    /\ UNCHANGED <<Time>>
    /\ UNCHANGED vars

(* Zave's counterexample relies on FinishStabilize preserving the rest
of the successor list when stabilizing. *)
FaultyFinishStabilize(self) ==
    /\ HasJoined[self]
    /\ UNCHANGED <<Predecessor, HasJoined, HasPredecessor, HasFailed,
        SuccessorAnswers, SuccessorRequests,
        PredecessorRequests>>
    /\ \E Answer \in PredecessorAnswers[self]:
        /\ ~HasFailed[Answer.predecessor]
        /\ IF BetweenExclusive(Answer.predecessor, self, Head(Successors[self]))
            THEN
                (* Preserve the rest of the successor list: *)
                /\ Successors' = [Successors EXCEPT ![self] = 
                    IF Len(Successors[self]) > 1
                    THEN <<Answer.predecessor>> \o Tail(Successors[self])
                    ELSE <<Answer.predecessor>>]
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Answer.predecessor] =
                    @ \union {[origin |-> self]}]
            ELSE
                /\ Successors' = Successors
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Head(Successors[self])] =
                    @ \union {[origin |-> self]}]
                    
ValidSuccessorList ==
    /\ \A self \in ProcSet:
        (* If the node is live and has more than one successor, *)
        \/ HasFailed[self]
        \/ Len(Successors[self]) < 2
        (* there should be no live node v *)
        \/ ~\E m \in ProcSet:
            /\ HasJoined[m]
            /\ ~HasFailed[m]
            (* which is skipped by the successor list of the node, *)
            /\ BetweenExclusive(m, Successors[self][1], Successors[self][2])
            (* and is part of the successor list of an antecedent: *)
            /\ \E antecedent \in ProcSet:
                /\ Head(Successors[antecedent]) = self
                /\ Len(Successors[antecedent]) = 2
                /\ Successors[antecedent][2] = m

ExampleNext ==
    (* Node 1 and 3 form a ring: *)
    \/ /\ Step(0) /\ Init /\ Join(1)
    \/ /\ Step(1) /\ FindSuccessor(3)
    \/ /\ Step(2) /\ FinishJoin(1)
    \/ /\ Step(3) /\ Stabilize(1)
    \/ /\ Step(4) /\ GetPredecessor(3)
    \/ /\ Step(5) /\ FaultyFinishStabilize(1)
    \/ /\ Step(6) /\ Notify(3)
    \/ /\ Step(7) /\ Stabilize(3)
    \/ /\ Step(8) /\ GetPredecessor(3)
    \/ /\ Step(9) /\ FaultyFinishStabilize(3)
    \/ /\ Step(10) /\ Notify(1)
    (* Node 5 joins and is incorporated into the ring: *)
    \/ /\ Step(11) /\ Join(5)
    \/ /\ Step(12) /\ FindSuccessor(3)
    \/ /\ Step(13) /\ FinishJoin(5)
    \/ /\ Step(14) /\ Stabilize(5)
    \/ /\ Step(15) /\ GetPredecessor(1)
    \/ /\ Step(16) /\ FaultyFinishStabilize(5)
    \/ /\ Step(17) /\ Notify(1)
    \/ /\ Step(18) /\ Stabilize(1)
    \/ /\ Step(19) /\ GetPredecessor(3)
    \/ /\ Step(20) /\ FaultyFinishStabilize(1)
    \/ /\ Step(21) /\ Notify(3)
    \/ /\ Step(22) /\ Stabilize(3)
    \/ /\ Step(23) /\ GetPredecessor(1)
    \/ /\ Step(24) /\ FaultyFinishStabilize(3)
    \/ /\ Step(25) /\ Notify(5)
    (* Node 2 joins but is not incorporated into the ring: *)
    \/ /\ Step(26) /\ Join(2)
    \/ /\ Step(27) /\ FindSuccessor(3)
    \/ /\ Step(28) /\ FindSuccessor(5)
    \/ /\ Step(29) /\ FindSuccessor(1)
    \/ /\ Step(30) /\ FinishJoin(2)
    (* Node 2 then fixes its successor list: *)
    \/ /\ Step(31) /\ FixSuccessorList(2)
    (* Node 4 joins and is incorporated into the network: *)
    \/ /\ Step(32) /\ Join(4)
    \/ /\ Step(33) /\ FindSuccessor(3)
    \/ /\ Step(34) /\ FinishJoin(4)
    \/ /\ Step(35) /\ Stabilize(4)
    \/ /\ Step(36) /\ GetPredecessor(5)
    \/ /\ Step(37) /\ FaultyFinishStabilize(4)
    \/ /\ Step(38) /\ Notify(5)
    \/ /\ Step(39) /\ Stabilize(3)
    \/ /\ Step(40) /\ GetPredecessor(5)
    \/ /\ Step(41) /\ FaultyFinishStabilize(3)
    \/ /\ Step(42) /\ Notify(4)
    (* Node 1 fixes its successor list: *)
    \/ /\ Step(43) /\ FixSuccessorList(1)
    (* Node 2 is finally incorporated into the ring: *)
    \/ /\ Step(44) /\ Stabilize(2)
    \/ /\ Step(45) /\ GetPredecessor(3)
    \/ /\ Step(46) /\ FaultyFinishStabilize(2)
    \/ /\ Step(47) /\ Notify(3)
    \/ /\ Step(48) /\ Stabilize(1)
    \/ /\ Step(49) /\ GetPredecessor(3)
    \/ /\ Step(50) /\ FaultyFinishStabilize(1)
    \/ /\ Step(51) /\ Notify(2)
    \/ /\ Done(52) /\ ~ValidSuccessorList
    

Counterexample == ExampleInit /\ [][ExampleNext]_<<Time, vars>>  
=============================================================================
\* Modification History
\* Last modified Thu May 09 22:42:20 CEST 2019 by lund
\* Created Wed Apr 17 13:55:32 CEST 2019 by lund

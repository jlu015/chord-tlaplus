---------------------- MODULE FaultTolerancePlayground ----------------------
EXTENDS Chord2003, TLC
ASSUME N = 4
ASSUME SuccessorsPerNode = 2
VARIABLES Time

ExampleInit == 
    /\ Time = 0
    /\ Successors = <<<<2, 3>>, <<3, 4>>, <<4, 1>>, <<1, 2>>>>
    /\ Predecessor = <<4, 1, 2, 3>>
    /\ HasJoined = <<TRUE, TRUE, TRUE, TRUE>>
    /\ HasPredecessor = <<TRUE, TRUE, TRUE, TRUE>>
    /\ HasFailed = <<FALSE, FALSE, FALSE, FALSE>>
    /\ SuccessorAnswers = [self \in ProcSet |-> {}]
    /\ SuccessorRequests = [self \in ProcSet |-> {}]
    /\ PredecessorAnswers = [self \in ProcSet |-> {}]
    /\ PredecessorRequests = [self \in ProcSet |-> {}]
    /\ Notifications = [self \in ProcSet |-> {}]

Step(y) == 
    /\ Time = y 
    /\ Time' = y + 1
           
Done(y) == 
    /\ Time = y
    /\ UNCHANGED <<Time>>
    /\ UNCHANGED vars
            
ExampleNext == 
    \/ /\ Step(0) /\ ValidRing /\ Stabilize(1)
    \/ /\ Step(1) /\ GetPredecessor(2)
    \/ /\ Step(2) /\ FinishStabilize(1)
    \/ /\ Done(3) /\ ValidRing  

Counterexample == ExampleInit /\ [][ExampleNext]_<<Time, vars>>
=============================================================================
\* Modification History
\* Last modified Wed May 01 21:55:48 CEST 2019 by lund
\* Created Tue Apr 30 09:33:45 CEST 2019 by lund

----------------------------- MODULE Chord2003 -----------------------------
(* Models the pure-join Chord stabilization algorithm
as specified in the 2001 SIGCOMM paper, using asynchronous communication
between nodes and fail-stop failures. *)

EXTENDS Integers, FiniteSets, Sequences, TLC
CONSTANTS 
    N, (* Number of nodes in the model *)
    SuccessorsPerNode, (* Length of nodes' successor lists *) 
    UseInitialRing (* FALSE if we should examine all possible configurations of successor pointers *)
ASSUME FiniteNodesAssumption == N \in Nat \ {0}
ASSUME FiniteNumberOfSuccessors == SuccessorsPerNode \in Nat \ {0}
ASSUME LessSuccessorsThanNodes == SuccessorsPerNode <= N
ASSUME InitialRingBoolean == UseInitialRing \in BOOLEAN
ProcSet == 1..N

VARIABLES Successors,
    Predecessor,
    HasJoined,
    HasPredecessor,
    HasFailed,
    SuccessorAnswers, 
    SuccessorRequests,
    PredecessorAnswers, 
    PredecessorRequests,
    Notifications
vars == <<Successors, Predecessor, HasJoined, HasPredecessor, HasFailed,
    SuccessorAnswers, SuccessorRequests,
    PredecessorAnswers, PredecessorRequests,
    Notifications>>

(* BetweenInclusive is true if x \in [a, b] (mod N).*)
BetweenInclusive(x, a, b) == ((x - a) % N) <= ((b - a) % N)

(* BetweenHalfOpen is true if x \in (a, b] (mod N).*)
BetweenHalfOpen(x, a, b) == 
    /\ ((x - a) % N) <= ((b - a) % N) /\ x # a

(* BetweenExclusive is true if x \in (a, b) (mod N) *)
BetweenExclusive(x, a, b) == 
    \/ /\ ((x - a) % N) <= ((b - a) % N) /\ x # a /\ x # b
    \/ /\ a = b

(* HasLiveSuccessor is true if there is at least one live successor in
the successor list of node x. *)
HasLiveSuccessor(x) == 
    \E succ \in 1..Len(Successors[x]) : ~HasFailed[Successors[x][succ]]

(* PickFirstLiveSuccessor is the index of the first live successor
in the successor list of node x. *)
PickFirstLiveSuccessor(x) == 
    CHOOSE succ \in 1..Len(Successors[x]) : 
            /\ ~HasFailed[Successors[x][succ]]
            (* There should not be any live successors earlier in the list: *)
            /\ ~\E cand \in 1..Len(Successors[x]):
                /\ ~HasFailed[Successors[x][cand]]
                /\ cand < succ

(* If node x is live, FirstLiveSuccessor is the identifier of the first live 
successor in the successor list of node x. If node x has failed,
FirstLiveSuccessor is the node's own identifier. *)
FirstLiveSuccessor(x) ==
    IF ~HasFailed[x]
    THEN Successors[x][PickFirstLiveSuccessor(x)]
    ELSE x

(* ReachableSuccessors is the set of nodes reachable from node x
when failed nodes are pruned from the successor lists.
If node x has failed, ReachableSuccessors is the empty set. *)    
ReachableSuccessors(x) ==
    LET RECURSIVE Reachable(_,_)
        Reachable(n,i) ==
            IF i = 0 \/ ~HasJoined[n] \/ HasFailed[n] \/ ~HasLiveSuccessor(n)
            THEN {}
            ELSE Reachable(FirstLiveSuccessor(n), i-1) 
                \cup {FirstLiveSuccessor(n)}
    IN Reachable(x, N)
    
FormsRing(x) == 
    /\ HasLiveSuccessor(x)
    /\ x \in ReachableSuccessors(x)

IsRingAppendage(x) == 
    /\ HasLiveSuccessor(x)
    /\ \E succ \in ReachableSuccessors(FirstLiveSuccessor(x)): FormsRing(succ)
    
ConnectedAppendages == \A self \in ProcSet: 
    \/ IsRingAppendage(self)
    \/ HasFailed[self]

AtLeastOneRing == \E x \in ProcSet: FormsRing(x) /\ ~HasFailed[x]

AtMostOneRing == ~\E x \in ProcSet: 
    /\ FormsRing(x)
    /\ \E y \in ProcSet:
        /\ FormsRing(y)
        /\ ReachableSuccessors(x) # ReachableSuccessors(y)

OrderedRing == 
    /\ LET ring == CHOOSE x \in ProcSet : FormsRing(x) /\ ~HasFailed[x] IN
        /\ \A self \in ReachableSuccessors(ring):
            \/ self < Head(Successors[self])
            \/ HasFailed[Head(Successors[self])]
            \/ ~\E n \in ReachableSuccessors(ring): 
                /\ n > Head(Successors[n]) 
                /\ self # n
                /\ ~HasFailed[Head(Successors[n])]

ValidRing == 
    /\ AtLeastOneRing
    /\ AtMostOneRing
    /\ ConnectedAppendages
    /\ OrderedRing

ValidInitialPredecessors ==
    /\ \E p \in ProcSet: HasPredecessor[p]
    /\ \A p \in ProcSet:
        \/ ~HasPredecessor[p]
        \/ /\ Head(Successors[Predecessor[p]]) = p
           /\ HasJoined[Predecessor[p]]

InitialRing ==
    /\ \E Start \in ProcSet:
        /\ Successors = [self \in ProcSet |-> <<Start>>]
        /\ Predecessor = [self \in ProcSet |-> Start]
        /\ HasJoined = [self \in ProcSet |-> self = Start]
        /\ HasPredecessor = [self \in ProcSet |-> self = Start]
    
Init == 
    /\ IF UseInitialRing 
        THEN InitialRing
        ELSE
        /\ Successors \in
            {[p \in ProcSet |-> <<initialSuccessor[p]>>] :
            initialSuccessor \in [ProcSet -> ProcSet]}
        /\ Predecessor \in [ProcSet -> ProcSet]
        /\ HasJoined \in [ProcSet -> BOOLEAN]
        /\ HasPredecessor \in [ProcSet -> BOOLEAN]
    /\ HasFailed = [self \in ProcSet |-> FALSE]
    /\ SuccessorAnswers = [self \in ProcSet |-> {}]
    /\ SuccessorRequests = [self \in ProcSet |-> {}]
    /\ PredecessorAnswers = [self \in ProcSet |-> {}]
    /\ PredecessorRequests = [self \in ProcSet |-> {}]
    /\ Notifications = [self \in ProcSet |-> {}]
    /\ ValidRing
    /\ ValidInitialPredecessors
    
(* Recursively find the successor for a given identifier. *)
FindSuccessor(self) ==
    /\ ~HasFailed[self]
    /\ ~HasFailed[Head(Successors[self])]
    /\ HasJoined[self]
    /\ UNCHANGED <<Successors, Predecessor, HasJoined, 
        HasPredecessor, HasFailed,  PredecessorAnswers, PredecessorRequests,
        Notifications>>
    /\ \E Request \in SuccessorRequests[self]:
        IF  \/ BetweenHalfOpen(Request.id, self, Head(Successors[self])) 
            \/ self = Head(Successors[self])
        THEN
            /\ SuccessorRequests' = [SuccessorRequests 
                EXCEPT ![self] = @ \ {Request}] 
            /\ SuccessorAnswers' = [SuccessorAnswers 
                EXCEPT ![Request.origin] =
                @ \union {[id |-> Request.id, successor |-> Head(Successors[self])]}]
        ELSE
            /\ SuccessorRequests' = [SuccessorRequests 
                EXCEPT ![Head(Successors[self])] =
                @ \union {Request}, ![self] = @ \ {Request}]
            /\ SuccessorAnswers' = SuccessorAnswers
                
Join(self) == 
    /\ ~HasFailed[Head(Successors[self])]
    /\ ~HasJoined[self]
    /\ Cardinality(SuccessorAnswers[self]) = 0
    /\ SuccessorRequests' =
        [SuccessorRequests EXCEPT ![Head(Successors[self])] =
        @ \union {[origin |-> self, id |-> self]}]
    /\ UNCHANGED <<Successors, Predecessor, HasJoined, HasFailed,
        HasPredecessor, SuccessorAnswers, PredecessorAnswers, 
        PredecessorRequests, Notifications>>

(* On receiving the successor for this node's identifier, 
set it as the successor: *)
FinishJoin(self) == 
    /\ ~HasJoined[self]
    /\ UNCHANGED <<Predecessor, HasPredecessor, HasFailed,
        SuccessorRequests,
        PredecessorAnswers, PredecessorRequests,
        Notifications>>
    /\ \E Answer \in SuccessorAnswers[self]:
        (* Ensure the node is live before adopting it as a successor.
        Necessary to avoid violating ConnectedAppendages. *)
        /\ ~HasFailed[Answer.successor]
        /\ Successors' = [Successors EXCEPT ![self] = <<Answer.successor>>]
        /\ HasJoined' = [HasJoined EXCEPT ![self] = TRUE]
        /\ SuccessorAnswers' = [SuccessorAnswers EXCEPT ![self] = @ \ {Answer}]

(* Periodically verify a node's immediate successor,
and tell the successor about the node, using the algorithm
from Figure 7 of the SIGCOMM paper. *)
Stabilize(self) ==
    /\ ~HasFailed[Head(Successors[self])]
    /\ HasJoined[self]
    /\ UNCHANGED <<Successors, Predecessor, HasJoined, HasPredecessor,
        HasFailed, SuccessorAnswers, SuccessorRequests,
        PredecessorAnswers,
        Notifications>>
    /\ PredecessorRequests' = [PredecessorRequests EXCEPT ![Head(Successors[self])] =
        @ \union {[origin |-> self]}]

(* Return the node's current predecessor. *)
GetPredecessor(self) ==
    /\ HasJoined[self]
    /\ HasPredecessor[self]
    /\ UNCHANGED <<Successors, Predecessor, HasJoined, HasPredecessor,
        HasFailed, SuccessorAnswers, SuccessorRequests,
        Notifications>>
    /\ \E Request \in PredecessorRequests[self]:
        /\ PredecessorRequests' = 
            [PredecessorRequests EXCEPT ![self] = @ \ {Request}]
        /\ PredecessorAnswers' =
            [PredecessorAnswers EXCEPT ![Request.origin] =
            @ \union {[id |-> self, predecessor |-> Predecessor[self]]}]
            
(* Once the node has received its successor's predecessor,
 determine whether it is a closer successor,*)
FinishStabilize(self) ==
    /\ HasJoined[self]
    /\ UNCHANGED <<Predecessor, HasJoined, HasPredecessor, HasFailed,
        SuccessorAnswers, SuccessorRequests,
        PredecessorRequests>>
    /\ \E Answer \in PredecessorAnswers[self]:
        /\ ~HasFailed[Answer.predecessor]
        /\ IF BetweenExclusive(Answer.predecessor, self, Head(Successors[self]))
            THEN
                (* Clear the successor list on adopting a new successor. *)
                /\ Successors' = [Successors EXCEPT ![self] = 
                    <<Answer.predecessor>>]
                (* To keep old successor list entries when stabilizing,
                replace the lines above with *)
\*              /\ Successors' = [Successors EXCEPT ![self] = 
\*                  <<Answer.predecessor>> \o Tail(Successors[self])] 
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Answer.predecessor] =
                    @ \union {[origin |-> self]}]
            ELSE
                /\ Successors' = Successors
                /\ PredecessorAnswers' = 
                    [PredecessorAnswers EXCEPT ![self] = {}]
                /\ Notifications' = [Notifications EXCEPT ![Head(Successors[self])] =
                    @ \union {[origin |-> self]}]

(* Accept a notification from another node
which thinks it might be our predecessor. *)
Notify(self) ==
    /\ HasJoined[self]
    /\ UNCHANGED <<Successors, HasJoined, HasFailed,
        SuccessorAnswers, SuccessorRequests,
        PredecessorAnswers, PredecessorRequests>>
    /\ \E Note \in Notifications[self]:
        /\ ~HasFailed[Note.origin]
        /\ IF 
            \/ ~HasPredecessor[self]
            \/ BetweenExclusive(Note.origin, Predecessor[self], self)
            THEN            
            /\ HasPredecessor' = [HasPredecessor EXCEPT ![self] = TRUE]
            /\ Predecessor' = [Predecessor EXCEPT ![self] = Note.origin]
            /\ Notifications' = [Notifications EXCEPT ![self] = @ \ {Note}]
            ELSE
            /\ UNCHANGED <<Successors, HasPredecessor, Predecessor>>
            /\ Notifications' = [Notifications EXCEPT ![self] = @ \ {Note}]

(* Instead of specifying fail-stop failures by permitting stuttering, we add
a separate action which verifies that the failure should be possible to
recover from, and marks the node as failed: *)
Fail(self) == 
    (* For a failure to be recoverable, every node should have at least one
    live node in its successor list after the failure: *)
    /\ \A member \in ProcSet:
        \/ member = self 
        \/ \E x \in 1..Len(Successors[member]): 
            /\ Successors[member][x] # self
            /\ ~HasFailed[Successors[member][x]]
    /\ HasFailed' = [HasFailed EXCEPT ![self] = TRUE]
    /\ UNCHANGED <<Successors, Predecessor, HasJoined, HasPredecessor,
        SuccessorAnswers, SuccessorRequests,
        PredecessorAnswers, PredecessorRequests,
        Notifications>>

(* FixPredecessor (AKA "flush")
detects that a node's predecessor has failed,
and marks it as no longer having a predecessor: *)
FixPredecessor(self) ==
    /\ HasFailed[Predecessor[self]]
    /\ HasJoined[self]
    /\ HasPredecessor[self]
    /\ HasPredecessor' = [HasPredecessor EXCEPT ![self] = FALSE]
    /\ UNCHANGED <<Successors, Predecessor, HasJoined,
        HasFailed, SuccessorAnswers, SuccessorRequests,
        PredecessorAnswers, PredecessorRequests,
        Notifications>>

(* FixSuccessor (AKA "update")
detects that a node's immediate successor has failed,
and replaces it with the next successor in the list: *)
FixSuccessor(self) ==
    /\ HasJoined[self]
    /\ HasFailed[Head(Successors[self])]
    /\ Successors' = [Successors EXCEPT ![self] = Tail(Successors[self])]
    /\ UNCHANGED <<Predecessor, HasJoined, HasPredecessor, HasFailed,
        SuccessorAnswers, SuccessorRequests,
        PredecessorAnswers, PredecessorRequests,
        Notifications>>

(* FixSuccessorList (AKA "reconcile") 
periodically fetches the successor list from the
immediate successor, prepends the successor to it,
and adopts it as the node's own successor list: *)
FixSuccessorList(self) ==
    /\ ~HasFailed[Head(Successors[self])]
    /\ HasJoined[self]
    /\ Head(Successors[self]) # self
    /\ LET 
        SuccessorListLength == 
            IF Len(Successors[Head(Successors[self])]) < SuccessorsPerNode 
            THEN Len(Successors[Head(Successors[self])])
            ELSE SuccessorsPerNode - 1
        NextSuccessorList == Successors[Head(Successors[self])]
       IN 
            /\ Head(NextSuccessorList) # Head(Successors[self])
            /\ Successors' = [Successors EXCEPT ![self] = 
                <<Head(Successors[self])>> \o 
                SubSeq(NextSuccessorList, 1, SuccessorListLength)]
    /\ UNCHANGED <<Predecessor, HasJoined, HasPredecessor, HasFailed,
        SuccessorAnswers, SuccessorRequests,
        PredecessorAnswers, PredecessorRequests,
        Notifications>>

Node(self) == 
    /\ ~HasFailed[self]
    /\  \/ FindSuccessor(self)
        \/ Join(self)
        \/ FinishJoin(self)
        \/ Stabilize(self)
        \/ GetPredecessor(self)
        \/ FinishStabilize(self)
        \/ Notify(self)
        \/ Fail(self)
        \/ FixPredecessor(self)
        \/ FixSuccessor(self)
        \/ FixSuccessorList(self)

RingIsWeaklyIdeal == 
    \A self \in ProcSet: 
        \/ HasFailed[self] 
        \/ /\ HasJoined[self] 
           /\ HasJoined[Predecessor[self]] 
           /\ Head(Successors[Predecessor[self]]) = self

Next == (\E self \in 1..N: Node(self))
    \/ /\ RingIsWeaklyIdeal /\ UNCHANGED vars

Spec == /\ Init /\ [][Next]_vars
        /\ \A self \in 1..N : WF_vars(Node(self))
  
TypeOK == 
    /\ \A self \in ProcSet:
        /\ Len(Successors[self]) > 0
        /\ Len(Successors[self]) <= SuccessorsPerNode 
        /\ \A s \in DOMAIN Successors[self]: Successors[self][s] \in ProcSet
        /\ Predecessor[self] \in ProcSet
        /\ HasJoined[self] \in BOOLEAN
        /\ HasPredecessor[self] \in BOOLEAN
        /\ IsFiniteSet(SuccessorAnswers[self])
        /\ IsFiniteSet(SuccessorRequests[self])
        /\ IsFiniteSet(PredecessorAnswers[self])
        /\ IsFiniteSet(PredecessorRequests[self])
        /\ IsFiniteSet(Notifications[self])


NoRingBecomesIdeal == ~RingIsWeaklyIdeal

=============================================================================
\* Modification History
\* Last modified Thu May 09 21:06:55 CEST 2019 by lund
\* Created Wed Apr 10 13:07:08 CEST 2019 by lund

------------------ MODULE OrderedAppendagesCounterexample ------------------
(* Models the counterexample to the OrderedAppendages property
from (4b) in definition 5.6 in the 2002 paper by Liben-Nowell et al.,
which states that

    "For every node v in the appendage Au (from the node u),
    the path of successors from v to u is increasing."

Zave informally summarizes this property as

    "members are ordered correctly within an appendage."
          
*)
EXTENDS Chord2003
ASSUME N = 4
ASSUME SuccessorsPerNode = 2
ASSUME UseInitialRing = FALSE
VARIABLES Time

ExampleInit == 
    /\ Time = 0
    /\ Successors = <<<<4>>, <<4>>, <<4>>, <<4>>>>
    /\ Predecessor = <<4, 4, 4, 4>>
    /\ HasJoined = <<FALSE, FALSE, FALSE, TRUE>>
    /\ HasPredecessor = <<FALSE, FALSE, FALSE, TRUE>>
    /\ HasFailed = <<FALSE, FALSE, FALSE, FALSE>>
    /\ SuccessorRequests = <<{}, {}, {}, {}>>
    /\ SuccessorAnswers = <<{}, {}, {}, {}>>
    /\ PredecessorRequests = <<{}, {}, {}, {}>>
    /\ PredecessorAnswers = <<{}, {}, {}, {}>>
    /\ Notifications = <<{}, {}, {}, {}>>
    /\ Init
  
Step(y) == 
    /\ Time = y 
    /\ Time' = y + 1
           
Done(y) == 
    /\ Time = y
    /\ UNCHANGED <<Time>>
    /\ UNCHANGED vars

OrderedAppendages ==
    /\ \A self \in ProcSet:
        (* Any node either has not joined the ring, *)
        \/ ~HasJoined[self]
        \/ ~HasJoined[Head(Successors[self])]
        \/ ~HasJoined[Head(Successors[Head(Successors[self])])]
        (* or is part of the cycle, *)
        \/ FormsRing(self)
        \/ FormsRing(Head(Successors[self]))
        (* or is part of an appendage with strictly ascending identifiers: *)
        \/ BetweenExclusive(
            Head(Successors[self]), 
            self, 
            Head(Successors[Head(Successors[self])]))

ExampleNext ==
    (* Node 2 and 4 form a ring: *)
    \/ /\ Step(0) /\ Join(2)
    \/ /\ Step(1) /\ FindSuccessor(4)
    \/ /\ Step(2) /\ FinishJoin(2)
    \/ /\ Step(3) /\ Stabilize(2)
    \/ /\ Step(4) /\ GetPredecessor(4)
    \/ /\ Step(5) /\ FinishStabilize(2)
    \/ /\ Step(6) /\ Notify(4)
    (* Node 3 joins: *)
    \/ /\ Step(7) /\ Join(3)
    \/ /\ Step(8) /\ Stabilize(4)
    \/ /\ Step(9) /\ FindSuccessor(4)
    \/ /\ Step(10) /\ FinishJoin(3)
    \/ /\ Step(11) /\ Stabilize(3)
    \/ /\ Step(12) /\ GetPredecessor(4)
    \/ /\ Step(13) /\ GetPredecessor(4)
    \/ /\ Step(14) /\ FinishStabilize(3)
    \/ /\ Step(15) /\ Notify(4)
    \/ /\ Step(16) /\ Join(1)
    \/ /\ Step(17) /\ FindSuccessor(4)
    \/ /\ Step(18) /\ FinishJoin(1)
    \/ /\ Step(19) /\ FinishStabilize(4)
    \/ /\ Step(20) /\ Notify(2)
    \/ /\ Step(21) /\ FixSuccessorList(3)
    \/ /\ Step(22) /\ FixSuccessorList(2)
    \/ /\ Step(23) /\ Stabilize(1)
    \/ /\ Step(24) /\ GetPredecessor(4)
    \/ /\ Step(25) /\ FinishStabilize(1)
    (* Node 4 fails, and node 2 and 3 correct their successor lists: *)
    \/ /\ Step(26) /\ Fail(4)
    \/ /\ Step(27) /\ FixSuccessor(2)
    \/ /\ Step(28) /\ FixSuccessor(3)
    (* Node 1 is now succeeded by node 3, which is succeeded by node 2.
    Since node 1 and 3 are part of an appendage, and 2 forms a ring,
    the invariant is violated. *)
    \/ /\ Done(29) /\ ~OrderedAppendages
    
Counterexample == ExampleInit /\ [][ExampleNext]_<<Time, vars>>
  
=============================================================================
\* Modification History
\* Last modified Thu May 09 15:57:28 CEST 2019 by lund
\* Created Wed Apr 17 13:55:18 CEST 2019 by lund

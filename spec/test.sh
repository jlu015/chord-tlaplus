#!/bin/bash
set -euo pipefail

# VERIFICATION OF THE CHORD PROTOCOL IN TLA+
# INF-3990 Master's Thesis in Computer Science
# Jørgen Aarmo Lund 15.05.2019
# 
# This script model-checks the specifications for small N
# and short trace lengths, and shows that the final specification
# admits Zave's counterexamples.
JAVA_CMD="java" 
TLC_CMD="-jar tla2tools.jar"

# How to invoke TLC when checking counterexamples:
TLC_COUNTEREXAMPLE_CMD="-jar ../tla2tools.jar"

# These arguments are normally set by TLA+ Toolbox.
# They may need to be changed for your PC!
JAVA_ARGS="-XX:MaxDirectMemorySize=10726m -Xmx5365m -XX:+IgnoreUnrecognizedVMOptions"
TLC_ARGS="-workers 4"

echo "==============================================================="
echo "Checking safety and liveness properties of synchronous Chord..."
echo "==============================================================="
${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -config SynchronousChord.cfg SynchronousChord.tla
echo "==============================================================="
echo "Checking safety properties of pure-join Chord..."
echo "==============================================================="
${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -config PureJoinChord.cfg PureJoinChord.tla 
echo "==============================================================="
echo "Checking basic type safety of Stoica's Chord for N=3"
echo "and traces up to 20 steps in length..."
echo "==============================================================="
${JAVA_CMD} ${JAVA_ARGS} ${TLC_CMD} ${TLC_ARGS} -config Chord2003.cfg -dfid 20 Chord2003.tla 
echo "==============================================================="
echo "Reproducing counterexamples to claimed Chord invariants..."
echo "==============================================================="
cd Counterexamples
echo "---------------------------------------------------------------"
echo "Retracing AtLeastOneRing counterexample..."
echo "---------------------------------------------------------------"
${JAVA_CMD} ${JAVA_ARGS} ${TLC_COUNTEREXAMPLE_CMD} ${TLC_ARGS} -config AtLeastOneRingCounterexample.cfg AtLeastOneRingCounterexample.tla 
echo "---------------------------------------------------------------"
echo "Retracing AtMostOneRing counterexample..."
echo "---------------------------------------------------------------"
${JAVA_CMD} ${JAVA_ARGS} ${TLC_COUNTEREXAMPLE_CMD} ${TLC_ARGS} -config AtMostOneRingCounterexample.cfg AtMostOneRingCounterexample.tla 
echo "---------------------------------------------------------------"
echo "Retracing ConnectedAppendages counterexample..."
echo "---------------------------------------------------------------"
${JAVA_CMD} ${JAVA_ARGS} ${TLC_COUNTEREXAMPLE_CMD} ${TLC_ARGS} -config ConnectedAppendagesCounterexample.cfg ConnectedAppendagesCounterexample.tla 
echo "---------------------------------------------------------------"
echo "Retracing OrderedAppendages counterexample..."
echo "---------------------------------------------------------------"
${JAVA_CMD} ${JAVA_ARGS} ${TLC_COUNTEREXAMPLE_CMD} ${TLC_ARGS} -config OrderedAppendagesCounterexample.cfg OrderedAppendagesCounterexample.tla 
echo "---------------------------------------------------------------"
echo "Retracing OrderedRing counterexample..."
echo "---------------------------------------------------------------"
${JAVA_CMD} ${JAVA_ARGS} ${TLC_COUNTEREXAMPLE_CMD} ${TLC_ARGS} -config OrderedRingCounterexample.cfg OrderedRingCounterexample.tla 
echo "---------------------------------------------------------------"
echo "Retracing OrderedMerges counterexample..."
echo "---------------------------------------------------------------"
${JAVA_CMD} ${JAVA_ARGS} ${TLC_COUNTEREXAMPLE_CMD} ${TLC_ARGS} -config OrderedMergesCounterexample.cfg OrderedMergesCounterexample.tla 
echo "---------------------------------------------------------------"
echo "Retracing ValidSuccessorList counterexample..."
echo "---------------------------------------------------------------"
${JAVA_CMD} ${JAVA_ARGS} ${TLC_COUNTEREXAMPLE_CMD} ${TLC_ARGS} -config ValidSuccessorListCounterexample.cfg ValidSuccessorListCounterexample.tla 
echo "==============================================================="
echo "FINISHED!"
echo "We have now verified pure-join Chord's correctness for small N"
echo "and shown that the full Chord specification admits Zave's"
echo "counterexamples."
echo "==============================================================="
-------------------------- MODULE SynchronousChord --------------------------
(* Models pure-join Chord with no node failures and synchronous communication, 
such that at most two nodes are communicating at any given time.
This permits very straightforward definitions of actions,
at the cost of introducing unrealistic assumptions. *)

EXTENDS Integers, FiniteSets, Sequences, TLC
CONSTANTS 
    N, (* The number of nodes in the model *)
    UseInitialRing (* FALSE if we should examine all possible configurations of successor pointers *)
ASSUME FiniteNodesAssumption == N \in Nat \ {0}
ASSUME UseInitialRingAssumption == UseInitialRing \in BOOLEAN

(* Use ascending natural numbers for the node identifiers,
since the consistent hashing scheme is irrelevant to the stabilization 
protocol: like the original SIGCOMM paper, we assume that the key space is
large enough that identifier collisions are unlikely. *)
ProcSet == 1..N

VARIABLES Successor,
    Predecessor,
    HasJoined,
    HasPredecessor
vars == <<Successor, Predecessor, HasJoined, HasPredecessor>>

(* We begin by defining convenience predicates 
to deal with identifier order. 

BetweenInclusive is true if x \in [a, b] (mod N). *)
BetweenInclusive(x, a, b) == ((x - a) % N) <= ((b - a) % N)

(* BetweenExclusive is true if x \in (a, b) (mod N) *)
BetweenExclusive(x, a, b) == 
    \/ /\ ((x - a) % N) <= ((b - a) % N) /\ x # a /\ x # b
    (* Caveat: in the case where a=b, as in a one-node ring, 
    we want to interpret (a,b) as spanning the entire interval: *)
    \/ /\ a = b
    
(* Determine which nodes are reachable from node x by following successive
successor pointers. Since TLC doesn't permit evaluating recursive operators 
over potentially infinite sequences, we explicitly bound the depth to the
number of reachable nodes, which is N for an N-node ring. *)
ReachableSuccessors(x) ==
    LET RECURSIVE Reachable(_,_)
        Reachable(n,i) ==
            IF i = 0 \/ ~HasJoined[n]
            THEN {}
            ELSE Reachable(Successor[n], i-1) \cup {Successor[n]}
    IN Reachable(x, N)

(* Define the network safety invariants, as they are needed to determine
the set of initial states: 

A node x forms a ring if it can reach itself through following
successor pointers, starting from itself: *)    
FormsRing(x) == x \in ReachableSuccessors(x)

(* A node is an appendage of the ring if it can reach at least one node forming
a ring by following successor pointers:  *)
IsRingAppendage(x) == 
    \E succ \in ReachableSuccessors(Successor[x]): FormsRing(succ)

(* The Chord network should have at least one ring: *)    
AtLeastOneRing == \E x \in ProcSet: FormsRing(x)

(* The Chord network should have at most one ring: the set of nodes reachable
through successor pointers should be equal for all nodes which form rings. *)
AtMostOneRing == ~\E x \in ProcSet: 
    /\ FormsRing(x)
    /\ \E y \in ProcSet:
        /\ FormsRing(y)
        /\ ReachableSuccessors(x) # ReachableSuccessors(y)

(* In an ordered ring, only one node should have a successor with a
smaller identifier than its own. *)        
OrderedRing == 
    LET ring == CHOOSE x \in ProcSet : FormsRing(x) IN
        /\ \A self \in ReachableSuccessors(ring):
            \/ self < Successor[self]
            \/ ~\E n \in ReachableSuccessors(ring): n > Successor[n] /\ self # n

(* The four invariants above are necessary to maintain correctness: *)    
ValidRing == 
        /\ AtLeastOneRing
        /\ AtMostOneRing
        /\ \A self \in ProcSet: IsRingAppendage(self)
        /\ OrderedRing

(* We allow any initial configuration which maintains the safety invariants.

For N > 5, TLC fails if we attempt to enumerate all possible
combinations of successor and predecessor pointers to find
initial states - the UseInitialRing setting instead
uses a smaller set of initial states, where one node forms
a ring and the rest join it. *)
Init == /\ IF UseInitialRing 
            THEN
            /\ \E Start \in ProcSet:
                /\ Successor = [self \in ProcSet |-> Start]
            ELSE    
            /\ Successor \in [ProcSet -> ProcSet]
        /\ Predecessor = [self \in ProcSet |-> 1]
        /\ HasJoined \in [ProcSet -> BOOLEAN]
        /\ HasPredecessor = [self \in ProcSet |-> FALSE]
        /\ ValidRing

(* Next, we define the actions making up the Chord stabilization protocol: 

Nodes join the network by learning the successor node for their identifier
from a member of the network, and setting it as their successor.*)               
Join(self) == 
    /\ ~HasJoined[self]
    /\ HasJoined' = [HasJoined EXCEPT ![self] = TRUE]
    (* After joining, there are no nodes lying 
    between this node's identifier and its successor's: *)
    /\ Successor' = [Successor EXCEPT ![self] = 
        CHOOSE succ \in {Successor[self]} 
            \cup ReachableSuccessors(Successor[self]):
            /\ ~\E candidate \in ReachableSuccessors(self):
                /\ BetweenExclusive(self, candidate, succ)
                /\ candidate # succ]
    /\ UNCHANGED <<Predecessor, HasPredecessor>>

(* Each nodes periodically stabilize, setting the successor's predecessor
as its new successor if it lies closer in identifier order.  *)              
Stabilize(self) == 
    /\ HasJoined[self]
    /\ HasPredecessor[Successor[self]]
    /\ BetweenExclusive(Predecessor[Successor[self]], self, Successor[self])
    /\ Successor' = [Successor EXCEPT ![self] = Predecessor[Successor[self]]]
    /\ UNCHANGED <<HasJoined, HasPredecessor, Predecessor>>

(* After a notification, a node similarly determines whether to adopt its
predecessor's successor as its new predecessor: *)
Notify(self) == 
    /\ HasJoined[self]
    /\ \/ ~HasPredecessor[Successor[self]]
       \/ BetweenExclusive(self, Predecessor[Successor[self]], Successor[self]) 
    /\ Predecessor' = [Predecessor EXCEPT ![Successor[self]] = self]
    /\ HasPredecessor' = [HasPredecessor EXCEPT ![Successor[self]] = TRUE]
    /\ UNCHANGED <<Successor, HasJoined>>

(* Joining, stabilizing and notifying are sufficient to specify
the stabilization protocol: *)    
Node(self) == 
    \/ Join(self)
    \/ Stabilize(self)
    \/ Notify(self)

(* We define one safety invariant, the type invariant: *)        
TypeOK == /\ \A self \in ProcSet: Successor[self] \in ProcSet
          /\ \A self \in ProcSet: Predecessor[self] \in ProcSet
          /\ \A self \in ProcSet: HasJoined[self] \in BOOLEAN
          /\ \A self \in ProcSet: HasPredecessor[self] \in BOOLEAN
          
(* Next, we define liveness properties. 

In a weakly ideal ring (as defined in the 2002 paper by Liben-Nowell et al.),
every node is the predecessor of its successor, and the network is stable. *)
RingIsWeaklyIdeal == 
    \A self \in ProcSet: 
        /\ HasJoined[self]
        /\ HasPredecessor[self]
        /\ Successor[Predecessor[self]] = self
          
RingEventuallyBecomesWeaklyIdeal == [](ValidRing => <>[]RingIsWeaklyIdeal)

(* In a strongly ideal ring, there are additionally no nodes between a node 
and its successor: *) 
RingIsStronglyIdeal ==
    /\ RingIsWeaklyIdeal
    /\ \A self \in ProcSet: 
        /\ ~\E v \in ProcSet: BetweenExclusive(v, self, Successor[self])

(* Zave's pure-join correctness theorem states that any valid network state
will eventually progress to an ideal one, and remain in it: *)
RingEventuallyBecomesStronglyIdeal == [](ValidRing => <>[]RingIsStronglyIdeal)

(* We add a disjunct to the next-state action to allow stuttering once
the network is strongly ideal, avoiding a deadlock error: *)
Next == (\E self \in 1..N: Node(self))
    \/ /\ RingIsStronglyIdeal /\ UNCHANGED vars

(* We use weak fairness as the specification's fairness condition: *)
Spec == /\ Init /\ [][Next]_vars
        /\ \A self \in 1..N : WF_vars(Node(self))

=============================================================================

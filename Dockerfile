FROM debian

# Set up dependencies
RUN apt-get update && apt-get install -y wget \
	unzip \
    openjdk-8-jre-headless

# Set up TLC:
RUN ["wget", "--quiet", "https://github.com/tlaplus/tlaplus/releases/download/v1.5.7/tla.zip", "-O", "/tmp/tla.zip"]
WORKDIR /tmp/
RUN unzip tla.zip -d /opt/
ENV CLASSPATH="${CLASSPATH}:/opt/tla/"

RUN mkdir /spec/
COPY spec/ /spec/
WORKDIR /spec/
CMD /spec/dockertest.sh
